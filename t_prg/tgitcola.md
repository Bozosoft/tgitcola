# tgitcola : Notes pour Git-cola

Page d'information pour [Utliser Git-Cola sous Linux-Debian ](http://jc.etiemble.free.fr/abc/index.php/trucs-astuces/git_linux0/git-cola-linux "Copies d'écran pour l'utilisation")

## Note sur la traduction
- Traduction du fichier du 20/10/2023 (36f1a1b) sur la base du fichier [de langue fr](https://github.com/git-cola/git-cola/blob/main/cola/i18n/fr.po) disponible sur le git de git-cola.

- J'ai effectué les modifications sur certaines traductions en particulier sur "marqueur" remplacé par "Tag" et quelques autres lignes non traduites,
- Le nouveau fichier traduit devient donc  [git-cola050124-fr.po]
- Ensuite il suffit de convertir le fichier .po (TEXTE) vers un fichier .mo (COMPILÉ) en utilisant le terminal
  - ...: $ msgfmt -o git-cola.mo -v git-cola050124-fr.po
- Le nouveau fichier [git-cola.mo]("Fichier compilé .mo ma version corrigée") doit alors être copié dans le dossier /usr/share/locale/fr/LC_MESSAGES/ pour que la traduction soit opérationnelle.
  - ...: $ sudo cp git-cola.mo /usr/share/locale/fr/LC_MESSAGES/

