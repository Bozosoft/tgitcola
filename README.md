# tgitcola : Tests pour Git-cola
- [Git-Cola](https://git-cola.github.io/ "The highly caffeinated Git GUI") Copyright (C) 2007-2023 David Aguilar and contributors
- Git-Cola est une interface utilisateur graphique pour Git, simple mais puissante, qui a été développé en Python

## Zone de tests
Zone de tests pour manipulations et essais de Git-Cola

![gitcola_image](http://jc.etiemble.free.fr/abc/uploads/images/aide_gitcola/gitcola_r.png "l'interface de Git-Cola")

Pour information [Utliser Git-Cola sous Linux-Debian ](http://jc.etiemble.free.fr/abc/index.php/trucs-astuces/git_linux0/git-cola-linux "Copies d'écran pour l'utilisation")


##  Changelog

    16/04/2023 - Modification pour ajout des informations et fichiers de langue fr
    01/03/2023 - Création du dossier
